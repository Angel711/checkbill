package net.pwa.checkbill.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.pwa.checkbill.dao.security.AuthenticationDAO;


@Component
public class AuthenticationService {

	@Autowired private AuthenticationDAO authenticationDAO;
	
	public Boolean validCredentials(final Object principal, final Object credentials) {
		return authenticationDAO.validCredentials(principal.toString(), credentials.toString());	
	}
}