package net.pwa.checkbill.enums.msg;

import lombok.AllArgsConstructor;
import lombok.Getter;



@Getter
@AllArgsConstructor
public enum SuccessMessage {

	// ================================== USERS ==================================
	PASSWORD_UPDATED("La contraseña ha sido actualizada correctamente.");

	private String message;
	public static String ESTATUS = "success";
}