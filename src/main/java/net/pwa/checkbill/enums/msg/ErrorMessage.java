package net.pwa.checkbill.enums.msg;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum ErrorMessage {
	
	// ================================== GENERAL ==================================
	UNDEFINED("Error general del sistema."),
	
	// ================================== USERS ==================================
	USER_LOCKED("Ocurrió un error al bloquear al usuario."),
	USER_LOGIN_ATTEMPTS_RESET("Ocurrió un error al reiniciar los intentos de inicio de sesión."),
	
	// ================================== USERS ==================================
	PASSWORD_UPDATED("Ocurrió un error al actualizar la contraseña.");

	private String message;
	public static String ESTATUS = "error";
	
	public static ErrorMessage getByMessage(String message) {
		for(ErrorMessage me : ErrorMessage.values()) {
			if(me.message.equalsIgnoreCase(message))
				return me;
		}
		
		return null;
	}
	
	public String toString() {
		return this.message;
	}
}