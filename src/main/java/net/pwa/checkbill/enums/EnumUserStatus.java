package net.pwa.checkbill.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum EnumUserStatus {

	ACTIVE("ACTIVE", "Activo"),
	LOCKED("LOCKED", "Bloqueado"),
	INACTIVE("INACTIVE", "Inactivo");
	
	private String status;
	private String spanish;
}