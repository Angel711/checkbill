package net.pwa.checkbill.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum EnumRoleUser {

	ROLE_ADMINISTRATOR(1L,"ROLE_ADMINISTRATOR", "Administrador"),
	ROLE_VALIDATOR(2L,"ROLE_VALIDATOR","Validador"),
	ROLE_CANDIDATE(3L,"ROLE_CANDIDATE","Apirante");
	
	private Long id;
	private String name;
	private String description;
}