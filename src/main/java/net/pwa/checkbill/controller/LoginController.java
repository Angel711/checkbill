package net.pwa.checkbill.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class LoginController extends SystemController {
	
	@RequestMapping("/login")
	public ModelAndView loginView(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		try {			
			model = createCustomView("login");
		} catch(Exception e) {
			saveSystemLog(model, request, e);
		}
		
		return model;
	}
}