package net.pwa.checkbill.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class DashboardController extends SystemController {

	@RequestMapping({"administrator/dashboard", "validator/dashboard", "cadidate/dashboard"})
	public ModelAndView loginView(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		try {			
			model = createCustomView("dashboard");
			model.addObject("currentDay", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
		} catch(Exception e) {
			saveSystemLog(model, request, e);
		}
		
		return model;
	}
}