package net.pwa.checkbill.controller.exception;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.pwa.checkbill.business.SystemLogBusiness;
import net.pwa.checkbill.enums.msg.ErrorMessage;
import net.pwa.checkbill.exception.CustomException;
import net.pwa.checkbill.model.SystemLog;
import net.pwa.checkbill.response.TransactionMessage;


@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class GlobalDefaultExceptionHandler {

	@Autowired private ObjectMapper basicObjectMapper;
	@Autowired private SystemLogBusiness systemLogBusiness;

	@SuppressWarnings("rawtypes")
	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity customErrorHandler(HttpServletRequest request, final Exception e) throws Exception {
		if(AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) 
			throw e;
		
		SystemLog systemLog = systemLogBusiness.save(request, e);
		TransactionMessage response = createResponseError(e, systemLog);
		response.setResponseCode(500);
		response.setResult(ErrorMessage.UNDEFINED.toString());
		
		return new ResponseEntity<String>(getJson(response), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@SuppressWarnings("rawtypes")
	@ExceptionHandler(CustomException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity defaultErrorHandler(HttpServletRequest request, final CustomException e) throws
            Exception {
		SystemLog bs = systemLogBusiness.save(request, e);
		TransactionMessage response = createResponseError(e, bs);
        response.setResult(bs.getExceptionMessage());
        
        return new ResponseEntity<String>(getJson(response), HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
	private TransactionMessage createResponseError(final Exception e, final SystemLog systemLog) {
		TransactionMessage response = new TransactionMessage();
        System.out.println(ExceptionUtils.getStackTrace(e));
        ErrorMessage msgError = ErrorMessage.getByMessage(systemLog.getExceptionMessage());
        if(msgError != null)
        	response.setResult(msgError.getMessage());
        response.setResponseCode(500);
        return response;
    }
	
	private String getJson(final TransactionMessage response) throws IOException {
        return basicObjectMapper.writeValueAsString(response);
    }
}