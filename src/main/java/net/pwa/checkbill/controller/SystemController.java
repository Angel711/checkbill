package net.pwa.checkbill.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;

import net.pwa.checkbill.business.SystemLogBusiness;
import net.pwa.checkbill.dao.UserDAO;
import net.pwa.checkbill.model.User;
import net.pwa.checkbill.security.CustomUserDetails;
import net.pwa.checkbill.vo.SessionVO;



public class SystemController {
	
	@Autowired private UserDAO userDAO;
	@Autowired private SystemLogBusiness systemLogBusiness;
	
	public ModelAndView createCustomView(String viewName) {
		return createCustomView(viewName, null);
	}
	
	public ModelAndView createCustomView(String viewName, SessionVO session) {
		if(session == null)
			session = getSession();
		
		ModelAndView model = new ModelAndView();
		model.addObject("session", session);
		model.setViewName(viewName);
		
		if(session.getUser() != null && (!session.getUser().getIsPasswordUpdated()))
			model.setViewName("password");
		
		return model;
	}

	private SessionVO getSession() {
		return new SessionVO(getUserLogin(), systemLogBusiness.getVersion());
	}

	private User getUserLogin() {
		CustomUserDetails userDetails = getLogin();
		
		if(userDetails != null)
			return userDAO.findOneById(userDetails.getIdUser());
			
		return null;
	}

	private CustomUserDetails getLogin() {
		try {
			return (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch(Exception e) {
			return null;
		}
	}

	public void saveSystemLog(ModelAndView model, HttpServletRequest request, Exception e) {
		if(model != null) {
			model.addObject("exception", e);
			model.addObject("message", e.getMessage());
			model.setViewName("error/construction");
		}
		
		e.printStackTrace();
		systemLogBusiness.save(request, e);
	}
}