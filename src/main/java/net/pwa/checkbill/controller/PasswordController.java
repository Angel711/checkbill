package net.pwa.checkbill.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.pwa.checkbill.business.PasswordBusiness;
import net.pwa.checkbill.enums.msg.ErrorMessage;
import net.pwa.checkbill.exception.CustomException;
import net.pwa.checkbill.response.TransactionMessage;
import net.pwa.checkbill.vo.model.PasswordVO;

/**
*
*/


@Controller
public class PasswordController extends SystemController {
	
	@Autowired private PasswordBusiness passwordBusiness;

	@ResponseBody
	@RequestMapping(value = { "administrator/password/update", "validator/password/update", "candidate/password/update" }, method = RequestMethod.POST)
	public TransactionMessage updatePassword(@ModelAttribute PasswordVO password) {
		try {			
			return PasswordBusiness.update(password);
		} catch(Exception e) {
			throw new CustomException(ErrorMessage.PASSWORD_UPDATED, e);
		}
	}
}
