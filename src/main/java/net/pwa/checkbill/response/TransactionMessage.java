package net.pwa.checkbill.response;

import lombok.AllArgsConstructor;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.pwa.checkbill.enums.msg.ErrorMessage;
import net.pwa.checkbill.enums.msg.SuccessMessage;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TransactionMessage {

	private Object data;
	private String status;
	private String result;
	private Integer responseCode;
	
	public TransactionMessage(ErrorMessage message) {
		super();
		this.status = ErrorMessage.ESTATUS;
		this.result = message.getMessage();
	}
	
	public TransactionMessage(ErrorMessage message, Object data) {
		super();
		this.data = data;
		this.status = ErrorMessage.ESTATUS;
		this.result = message.getMessage();
	}
	
	public TransactionMessage(SuccessMessage msg) {
		super();
		this.status = SuccessMessage.ESTATUS;
		this.result = msg.getMessage();
	}
	
	public TransactionMessage(SuccessMessage msg, Object data) {
		super();
		this.data = data;
		this.status = SuccessMessage.ESTATUS;
		this.result = msg.getMessage();
	}

	
//	public TransactionMessage(WarningMessage msg) {
//		super();
//		this.status = WarningMessage.ESTATUS;
//		this.result = msg.getMessage();
//	}
//	
//	public TransactionMessage(WarningMessage msg, Object data) {
//		super();
//		this.data = data;
//		this.status = WarningMessage.ESTATUS;
//		this.result = msg.getMessage();
//	}
//	
//	public TransactionMessage(SuccessMessage msg) {
//		super();
//		this.status = SuccessMessage.ESTATUS;
//		this.result = msg.getMessage();
//	}
//	
//	public TransactionMessage(SuccessMessage msg, Object data) {
//		super();
//		this.data = data;
//		this.status = SuccessMessage.ESTATUS;
//		this.result = msg.getMessage();
//	}
//	
//	public TransactionMessage(NotificationMessage msg) {
//		super();
//		this.status = NotificationMessage.ESTATUS;
//		this.result = msg.getMessage();
//	}
//	
//	public TransactionMessage(NotificationMessage msg, Object data) {
//		super();
//		this.data = data;
//		this.status = NotificationMessage.ESTATUS;
//		this.result = msg.getMessage();
//	}
}