package net.pwa.checkbill.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;


@Data
@Entity
@Table(name = "SYSTEM_LOG")
public class SystemLog implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@Column(name = "ID")
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SYSTEM_LOG")
	@SequenceGenerator(name = "SEQ_SYSTEM_LOG", sequenceName = "SEQ_SYSTEM_LOG", allocationSize = 1)
	private Long id;
	
	@NotNull
	@Size(max = 500)
	@Basic(optional = false)
	@Column(name = "ACTION")
	private String action;
	
	@NotNull
	@Basic(optional = false)
	@Column(name = "CONTEXT")
	private String context;
	
	@Basic(optional = false)
	@Column(name = "EXCEPTION_MESSAGE")
	private String exceptionMessage;
	
	@Basic(optional = false)
	@Column(name = "ID_USER")
	private Long idUser;
	
	@JoinColumn(name = "ID_USER", referencedColumnName = "ID", insertable = false, updatable = false, nullable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private User user;
	
	@NotNull
	@Basic(optional = false)
	@Column(name = "CREATED_AT")
	private @DateTimeFormat(pattern = "dd/MM/yyyy") Date createdAt;

	@Override
	public String toString() {
		return "System Log [id=" + id + ", action=" + action + ", exceptionMessage="
				+ exceptionMessage + ", user=" + ((user != null) ? user.getFullName() : "") + ", createdAt=" + createdAt + "]";
	}
}