package net.pwa.checkbill.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;


@Data
@Entity
@Table(name = "PASSWORD")
public class Password implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@Column(name = "ID_USER")
	@Basic(optional = false)
	private Long idUser;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_USER", referencedColumnName = "ID", insertable = false, updatable = false, nullable = false)
	private User user;
	
	@NotNull
	@Size(max = 1000)
	@Basic(optional = false)
	@Column(name = "PASSWORD")
	private String password;
	
	@NotNull
	@Basic(optional = false)
	@Column(name = "IS_UPDATED")
	private Boolean isUpload;
	
	@NotNull
	@Basic(optional = false)
	@Column(name = "CREATED_AT")
	private @DateTimeFormat(pattern = "dd/MM/yyyy") Date createdAt;
	
	@NotNull
	@Basic(optional = false)
	@Column(name = "UPDATED_AT")
	private @DateTimeFormat(pattern = "dd/MM/yyyy") Date updatedAt;
}