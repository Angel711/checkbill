package net.pwa.checkbill.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.pwa.checkbill.enums.EnumUserStatus;
import net.pwa.checkbill.vo.model.UserVO;


@Data
@Entity
@NoArgsConstructor
@Table(name = "UUSER")
@NamedQueries({
		@NamedQuery(name = "User.findOneByPhoneNumber", query = "SELECT u FROM User u WHERE u.phoneNumber = :phoneNumber"),
		@NamedQuery(name = "User.findOneByEmailAddress", query = "SELECT u FROM User u WHERE u.emailAddress = :emailAddress") 
})
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@Column(name = "ID")
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_USER")
	@SequenceGenerator(name = "SEQ_USER", sequenceName = "SEQ_USER", allocationSize = 1)
	private Long id;

	@NotNull
	@Size(max = 100)
	@Basic(optional = false)
	@Column(name = "FIRST_NAME")
	private String firstName;

	@NotNull
	@Size(max = 100)
	@Column(name = "LAST_NAME")
	@Basic(optional = false)
	private String lastName;

	@NotNull
	@Size(max = 200)
	@Basic(optional = false)
	@Column(name = "FULL_NAME")
	private String fullName;

	@NotNull
	@Size(max = 30)
	@Basic(optional = false)
	@Column(name = "PHONE_NUMBER", unique = true)
	private String phoneNumber;

	@NotNull
	@Size(max = 50)
	@Basic(optional = false)
	@Column(name = "EMAIL_ADDRESS", unique = true)
	private String emailAddress;

	@NotNull
	@Size(max = 20)
	@Column(name = "STATUS")
	@Basic(optional = false)
	private String status;

	@NotNull
	@Basic(optional = false)
	@Column(name = "LOGIN_ATTEMPTS")
	private Integer loginAttempts;

	@NotNull
	@Basic(optional = false)
	@Column(name = "IS_PASSWORD_UPDATED")
	private Boolean isPasswordUpdated;

	@NotNull
	@Basic(optional = false)
	@Column(name = "CREATED_AT")
	private @DateTimeFormat(pattern = "dd/MM/yyyy") Date createdAt;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "ROLE_USER", joinColumns = @JoinColumn(name = "ID_USER", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ID_ROLE", referencedColumnName = "ID"))
	private List<Role> roles;

	public User(UserVO user) {
		super();
		this.firstName = user.getFirstName().trim();
		this.lastName = user.getLastName().trim();
		this.fullName = user.getFirstName().trim() + " " + user.getLastName().trim();
		this.phoneNumber = user.getPhoneNumber().trim();
		this.emailAddress = user.getEmailAddress().trim();
		this.status = EnumUserStatus.ACTIVE.getStatus();
		this.loginAttempts = 0;
		this.isPasswordUpdated = false;
		this.createdAt = new Date();
	}
}