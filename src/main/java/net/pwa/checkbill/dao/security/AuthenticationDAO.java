package net.pwa.checkbill.dao.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import net.pwa.checkbill.business.UserBusiness;
import net.pwa.checkbill.dao.PasswordDAO;
import net.pwa.checkbill.enums.EnumUserStatus;
import net.pwa.checkbill.model.Password;
import net.pwa.checkbill.model.User;


@Repository
public class AuthenticationDAO {
	
	@Autowired private PasswordDAO passwordDAO;
	@Autowired private UserBusiness userBusiness;
	@Autowired private PasswordEncoder passwordEncoder;
	
	public Boolean validCredentials(String username, String password) {
		User user = userBusiness.findOneByPhoneNumberOrEmails(username);
		
		if (user != null) {	
			if(user.getStatus().equals(EnumUserStatus.INACTIVE.getStatus())) {
				throw new DisabledException("Authentication Service Reply.");
			} else {
				Password passwordDB = passwordDAO.findOneById(user.getId());
				
				if(passwordDB != null) {
					if(passwordDB.getIsUpload()) {
						return passwordEncoder.matches(password, passwordDB.getPassword());
					} else {
						return (passwordDB.getPassword().equals(password));
					}
				}
			}
		}
		
		return false;
	}
}