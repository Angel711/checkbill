package net.pwa.checkbill.dao;

import org.springframework.stereotype.Repository;

import net.pwa.checkbill.model.Password;


@Repository
public class PasswordDAO extends AbstractJPADAO<Password> {

	public PasswordDAO() {
		super(Password.class);
	}
}