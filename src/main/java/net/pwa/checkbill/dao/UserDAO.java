package net.pwa.checkbill.dao;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import net.pwa.checkbill.model.User;


@Repository
public class UserDAO extends AbstractJPADAO<User> {

	public UserDAO() {
		super(User.class);
	}
	
	public User findOneByPhoneNumber(String phoneNumber) {
		try {
			final TypedQuery<User> typedQuery = this.getEntityManager().createNamedQuery("User.findOneByPhoneNumber", User.class);
			typedQuery.setParameter("phoneNumber", phoneNumber);
			
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public User findOneByEmailAddress(String emailAddress) {
		try {
			final TypedQuery<User> typedQuery = this.getEntityManager().createNamedQuery("User.findOneByEmailAddress", User.class);
			typedQuery.setParameter("emailAddress", emailAddress);
			
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
}