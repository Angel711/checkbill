package net.pwa.checkbill.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
public class AbstractJPADAO<T> {
	
	private Class<T> clazz;
	
	@Getter 
	@PersistenceContext
	private EntityManager entityManager;
	
	public AbstractJPADAO(Class<T> clazz) {
		this.clazz = clazz;
	}
	
	public T findOneById(long id) {
		return entityManager.find(clazz, id);
	}
	
	public List<T> findAll(){
		return entityManager.createQuery("from " + clazz.getName(),clazz).getResultList();
	} 
	
	public void create(T entity) {
		entityManager.persist(entity);
	}
	
	public T update(T entity) {
		return entityManager.merge(entity);
	}

	public void delete(T entity) {
		entityManager.remove(entity);
	}
	
	public void deleteById(long id) {
		T entity = findOneById(id);
		entityManager.remove(entity);
	}
}