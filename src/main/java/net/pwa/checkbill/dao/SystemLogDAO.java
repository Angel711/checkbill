package net.pwa.checkbill.dao;

import org.springframework.stereotype.Repository;

import net.pwa.checkbill.model.SystemLog;


@Repository
public class SystemLogDAO extends AbstractJPADAO<SystemLog> {

	public SystemLogDAO() {
		super(SystemLog.class);
	}
}