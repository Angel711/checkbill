package net.pwa.checkbill.vo.model;

import java.io.Serializable;

import lombok.Data;



@Data
public class PasswordVO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long idUser;
	private String password;
	private String passwordConfirmation;
}
