package net.pwa.checkbill.vo.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.pwa.checkbill.model.User;


@Data
@NoArgsConstructor
public class UserVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String firstName;
	private String lastName;
	private String fullName;
	private String phoneNumber;
	private String emailAddress;
	private String status;
	private Integer loginAttempts;
	private Boolean isPasswordUpdated;
	private String createdAt;
	private List<RoleVO> roles;
	private List<Long> roleList;
	
	public UserVO(User user) {
		super();
		this.id = user.getId();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.fullName = user.getFullName();
		this.phoneNumber = user.getPhoneNumber();
		this.emailAddress = user.getEmailAddress();
		this.status = user.getStatus();
		this.loginAttempts = user.getLoginAttempts();
		this.isPasswordUpdated = user.getIsPasswordUpdated();
		this.createdAt = new SimpleDateFormat("dd/MM/yyyy").format(user.getCreatedAt());
		this.roles = RoleVO.getListFromList(user.getRoles());
	}
}