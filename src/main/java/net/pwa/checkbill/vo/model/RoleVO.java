package net.pwa.checkbill.vo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.pwa.checkbill.model.Role;


@Data
@NoArgsConstructor
public class RoleVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id; 
	private String name;
	private String description;
	
	public RoleVO(Role role) {
		super();
		this.id = role.getId();
		this.name = role.getName();
		this.description = role.getDescription();
	}
	
	public static List<RoleVO> getListFromList(List<Role> roles) {
		List<RoleVO> response = new ArrayList<>();
		
		for(Role role : roles)
			response.add(new RoleVO(role));
		
		return response;
	}
}