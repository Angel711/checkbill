package net.pwa.checkbill.vo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.pwa.checkbill.model.User;
import net.pwa.checkbill.vo.model.UserVO;


@Data
@NoArgsConstructor
public class SessionVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private UserVO user;
	private String version;
	private String year = new SimpleDateFormat("yyyy").format(new Date());

	public SessionVO(User user, String version) {
		super();
		if(user != null) {
			this.user = new UserVO(user);
		}
		this.version = version;
	}
}