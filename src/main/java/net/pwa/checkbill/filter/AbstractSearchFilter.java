package net.pwa.checkbill.filter;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AbstractSearchFilter {

	private Integer limit;
	private Integer offset;
	private Boolean isFirstLoad;
	
	public String getPattern(String original) {
		return new StringBuilder("%").append(original).append("%").toString();
	}
}