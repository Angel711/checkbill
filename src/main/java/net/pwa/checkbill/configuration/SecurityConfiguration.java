package net.pwa.checkbill.configuration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import net.pwa.checkbill.security.CustomAuthenticationProvider;
import net.pwa.checkbill.security.CustomUserDetailsService;
import net.pwa.checkbill.security.CustomWebAuthenticationFailureHandler;
import net.pwa.checkbill.security.CustomWebUrlAuthenticationSuccessHandler;


@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	public static final String LOGOUT_DENIED_PERMISSION = "/api/logout?logoutCause=deniedPermission";
	
	@Autowired private CustomWebAuthenticationFailureHandler customWebAuthenticationFailureHandler;
	@Autowired private CustomWebUrlAuthenticationSuccessHandler customWebUrlAuthenticationSuccessHandler;
	
	@Bean
	@Override
	public AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(getAuthenticationProvider());
	}
	
	@Override
	public void configure(WebSecurity wb) throws Exception {
		wb.ignoring()
			.antMatchers("/sources/**/*.{js, html, css, png, woff}");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.exceptionHandling()
			.accessDeniedHandler(createDeniedHandler())
			.and()
			.formLogin()
			.loginProcessingUrl("/api/authenticate")
			.loginPage("/login")
			.failureHandler(customWebAuthenticationFailureHandler)
			.successHandler(customWebUrlAuthenticationSuccessHandler)
			.usernameParameter("username")
			.passwordParameter("password")
			.permitAll()
			.and()
			.logout()
			.logoutUrl("/api/logout")
			.invalidateHttpSession(true)
			.clearAuthentication(true)
			.logoutSuccessHandler(new LogoutSuccessHandler() {
				
				@Override
				public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
						throws IOException, ServletException {
					Object logoutCause = request.getParameter("logoutCause");
					String causaLogoutQuery = (logoutCause == null) ? "logout" : logoutCause.toString();
					response.setStatus(HttpStatus.OK.value());
					response.sendRedirect(String.format("%s/%s?%s", request.getContextPath(), "", causaLogoutQuery));
					response.getWriter().flush();
					
				}
			})
			.deleteCookies("JSESSIONID")
			.permitAll()
			.and()
			.headers()
			.frameOptions()
			.sameOrigin()
			.and()
			.authorizeRequests()
			.antMatchers("/administrator/**").hasRole("ADMINISTRATOR")
			.antMatchers("/validator/**").hasRole("VALIDATOR")
			.antMatchers("/candidate/**").hasRole("CANDIDATE")
			.and()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
			.maximumSessions(1)
			.sessionRegistry(sessionRegistry())
			.maxSessionsPreventsLogin(false)
			.expiredUrl("/login?expiredSession")
			.and()
			.invalidSessionUrl("/login?expiredSession")
			.sessionFixation()
			.newSession()
			.and()
			.csrf()
			.disable();
	}
	
	@Bean
	public AccessDeniedHandler createDeniedHandler() {
		return new AccessDeniedHandler() {

			@Override
			public void handle(HttpServletRequest request, HttpServletResponse response,
					AccessDeniedException accessDeniedException) throws IOException, ServletException {
				response.sendRedirect(request.getContextPath() + LOGOUT_DENIED_PERMISSION);
			}	
		};
	}
	
	@Bean
	public AuthenticationProvider getAuthenticationProvider() {
		return new CustomAuthenticationProvider();
	}
	
	@Bean
	public UserDetailsService userDetailsService() {
		return new CustomUserDetailsService();
	}
	
	@Bean
	public HttpSessionEventPublisher httpSessionEventPublisher() {
		return new HttpSessionEventPublisher();
	}
	
	@Bean
	public SessionRegistry sessionRegistry() {
		return new SessionRegistryImpl();
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}