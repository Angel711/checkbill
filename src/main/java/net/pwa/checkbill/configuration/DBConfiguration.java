package net.pwa.checkbill.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;


public class DBConfiguration {
	
	@Value("${db.main.config.jdbcUrl}") private String jdbcUrl;
	@Value("${db.main.config.username}") private String username;
	@Value("${db.main.config.password}") private String password;
	@Value("${db.main.config.driverClassName}") private String driverClassName;
	@Value("${db.main.config.maximumPoolSize}") private Integer maximumPoolsize;
	
	@Bean(name="dataSource",destroyMethod="close")
	public DataSource getDataSourceOracle() {
		HikariConfig config = new HikariConfig();
		config.setDriverClassName(driverClassName);
		config.setJdbcUrl(jdbcUrl);
		config.setUsername(username);
		config.setPassword(password);
		config.setMaximumPoolSize(maximumPoolsize);
		config.setAutoCommit(false);
		
		return new HikariDataSource(config);
	}
	
	public JdbcTemplate jdbcTemplateOracle(@Qualifier("dataSource") DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}
}