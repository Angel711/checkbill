package net.pwa.checkbill.business;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import net.pwa.checkbill.dao.PasswordDAO;
import net.pwa.checkbill.dao.UserDAO;
import net.pwa.checkbill.enums.msg.ErrorMessage;
import net.pwa.checkbill.enums.msg.SuccessMessage;
import net.pwa.checkbill.model.Password;
import net.pwa.checkbill.model.User;
import net.pwa.checkbill.response.TransactionMessage;
import net.pwa.checkbill.vo.model.PasswordVO;



@Component
public class PasswordBusiness {
	
	@Autowired private UserDAO userDAO;
	@Autowired private PasswordDAO passwordDAO;
	@Autowired private PasswordEncoder passwordEncoder;

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public TransactionMessage update(PasswordVO password) {
		if(password.getIdUser() != null) {			
			Password passwordDB = passwordDAO.findOneById(password.getIdUser());
			
			if(passwordDB != null) {
				User user = userDAO.findOneById(password.getIdUser());
				user.setIsPasswordUpdated(true);
				userDAO.update(user);
				
				passwordDB.setPassword(passwordEncoder.encode(password.getPassword()));
				passwordDB.setIsUpload(true);
				passwordDB.setUpdatedAt(new Date());
				passwordDAO.update(passwordDB);
				
				return new TransactionMessage(SuccessMessage.PASSWORD_UPDATED);
			} else {
				User user = userDAO.findOneById(password.getIdUser());
				user.setIsPasswordUpdated(true);
				userDAO.update(user);
				
				passwordDB = new Password();
				passwordDB.setIdUser(password.getIdUser());
				passwordDB.setPassword(passwordEncoder.encode(password.getPassword()));
				passwordDB.setIsUpload(true);
				passwordDB.setCreatedAt(new Date());
				passwordDB.setUpdatedAt(new Date());
				passwordDAO.create(passwordDB);
				
				return new TransactionMessage(SuccessMessage.PASSWORD_UPDATED);
			}
		}
		
		return new TransactionMessage(ErrorMessage.PASSWORD_UPDATED);
	}

}
