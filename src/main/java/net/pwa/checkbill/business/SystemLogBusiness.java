package net.pwa.checkbill.business;

import java.util.Date;


import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import net.pwa.checkbill.dao.SystemLogDAO;
import net.pwa.checkbill.exception.CustomException;
import net.pwa.checkbill.model.SystemLog;
import net.pwa.checkbill.model.User;
import net.pwa.checkbill.security.CustomUserDetails;


@Component
public class SystemLogBusiness {
	
	@Autowired private SystemLogDAO systemLogDAO;
	@Autowired private UserBusiness userBusiness;
	
	@Value("${system.deploy.current.version}") private String VERSION;
	
	public String getVersion() {
		return VERSION;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public SystemLog save(HttpServletRequest request, final Exception e) {
		Date createdAt = new Date();
		String requestPath = request.getServletPath();
		SystemLog systemLog = new SystemLog();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if(auth != null) {
			if(auth.isAuthenticated()) {
				if(!"anonymousUser".equals(auth.getName()))
					systemLog.setIdUser(((CustomUserDetails) auth.getPrincipal()).getIdUser());
			} else
				setIdUserToSystemLog(request, systemLog);
		} else
			setIdUserToSystemLog(request, systemLog);
		
		if(e != null) {
			String exceptionMessage = e.getMessage();
			if(e instanceof CustomException) {
				CustomException ue = (CustomException) e;
				if(ue.getMessage() != null)
					exceptionMessage = ue.getMsgError().toString();
			}
			systemLog.setExceptionMessage(exceptionMessage);
			systemLog.setContext(ExceptionUtils.getStackTrace(e));
		}
		
		systemLog.setAction(requestPath);
		systemLog.setCreatedAt(createdAt);
		systemLogDAO.create(systemLog);
		
		return systemLog;
	}
	
	private void setIdUserToSystemLog(HttpServletRequest request, SystemLog systemLog) {
		String username = request.getParameter("username");
		
		if(username != null) {
			if(username.matches("^-?\\d+$")) {
				User user = null;
				
				try {
					user = userBusiness.findOneByPhoneNumberOrEmails(username);
				} catch(NumberFormatException e) {
					user = null;
				}
				
				if(user != null) {
					systemLog.setIdUser(user.getId());
				}
			}
		}
	}
}