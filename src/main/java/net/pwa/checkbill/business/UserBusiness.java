package net.pwa.checkbill.business;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import net.pwa.checkbill.dao.UserDAO;
import net.pwa.checkbill.enums.EnumUserStatus;
import net.pwa.checkbill.enums.msg.ErrorMessage;
import net.pwa.checkbill.exception.CustomException;
import net.pwa.checkbill.model.User;


@Component
public class UserBusiness{
	
	@Autowired private UserDAO userDAO;
	
	@Value("${system.maximum.login.attempts}") private Integer MAXIMUM_LOGIN_ATTEMPTS;
	
	public User findOneByPhoneNumberOrEmails(String user) {
		User userDB = null;

		if(user.contains("@")) {
			userDB = userDAO.findOneByEmailAddress(user);
		} else {
			userDB = userDAO.findOneByPhoneNumber(user);
		}

		return userDB;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void processAuthenticationFailure(String username) {
		try {
			User user = findOneByPhoneNumberOrEmails(username);

			if(user != null) {
				if(user.getLoginAttempts() == (MAXIMUM_LOGIN_ATTEMPTS - 1)) {
					user.setLoginAttempts(user.getLoginAttempts() + 1);
					user.setStatus(EnumUserStatus.LOCKED.getStatus());
				} else {
					user.setLoginAttempts(user.getLoginAttempts() + 1);
				}
				userDAO.update(user);
			}
		} catch (Exception e) {
			throw new CustomException(ErrorMessage.USER_LOCKED, e);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public User resetLoginAttempts(String user) {
		try {
			User userDB = findOneByPhoneNumberOrEmails(user);

			if(userDB != null && userDB.getLoginAttempts() != 0) {
				userDB.setLoginAttempts(0);
				userDAO.update(userDB);
			}

			return userDB;
		} catch (Exception e) {
			throw new CustomException(ErrorMessage.USER_LOGIN_ATTEMPTS_RESET, e);
		}
	}
}