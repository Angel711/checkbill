package net.pwa.checkbill.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import net.pwa.checkbill.business.UserBusiness;
import net.pwa.checkbill.enums.EnumUserStatus;
import net.pwa.checkbill.model.Role;
import net.pwa.checkbill.model.User;


public class CustomUserDetailsService implements UserDetailsService {
	
	@Autowired private UserBusiness userBusiness;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userBusiness.findOneByPhoneNumberOrEmails(username);
		
		if(user == null)
			return null;
		
		return createUserDetails(username, user, createGrantedAuthorities(user));
	}

	private UserDetails createUserDetails(String username, User usuario,
			List<GrantedAuthority> authorities) {
		CustomUserDetails customUserDetails;
		
		Boolean accountEnable = true;
		EnumUserStatus status = EnumUserStatus.valueOf(usuario.getStatus());
		
		if(!(status.equals(EnumUserStatus.LOCKED)))
			customUserDetails = new CustomUserDetails(username + " " + username, "noPassword", accountEnable, true, authorities);
		else
			customUserDetails = new CustomUserDetails(username, "", true, false, authorities);
		
		customUserDetails.setIdUser(usuario.getId());
		
		return customUserDetails;
	}

	private List<GrantedAuthority> createGrantedAuthorities(User user) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		for(Role role : user.getRoles())
			authorities.add(new SimpleGrantedAuthority(role.getName()));
		
		return authorities;
	}
}