package net.pwa.checkbill.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import net.pwa.checkbill.business.UserBusiness;
import net.pwa.checkbill.service.AuthenticationService;


public class CustomAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
	
	@Autowired private UserBusiness userBusiness;
	@Autowired private UserDetailsService userDetailsService;
	@Autowired private AuthenticationService authenticationService;
	
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		Boolean responseNipAuthentication = authenticationService.validCredentials(authentication.getPrincipal().toString().trim(), authentication.getCredentials().toString().trim());
		
		if(!responseNipAuthentication) {
			userBusiness.processAuthenticationFailure(authentication.getPrincipal().toString().trim());
			
			throw new BadCredentialsException("Authentication Service Reply: " + responseNipAuthentication);
		}
	}
	
	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		UserDetails user = userDetailsService.loadUserByUsername(username);
		
		if(user == null) {
			user = userDetailsService.loadUserByUsername(username);
			
			if(user != null)
				return user;
			else
				throw new BadCredentialsException("Authentication Service Reply.");
		}
		
		return user;
	}
}