package net.pwa.checkbill.security;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import net.pwa.checkbill.business.UserBusiness;
import net.pwa.checkbill.configuration.SecurityConfiguration;
import net.pwa.checkbill.enums.EnumRoleUser;
import net.pwa.checkbill.model.User;


@Component
public class CustomWebUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	
	@Autowired private UserBusiness userBusiness;
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		handler(request, response, authentication);
		clearAuthenticationAttributes(request);
	}

	private void handler(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException {
		User user = userBusiness.resetLoginAttempts(request.getParameter("username"));
		String targetUrl = determineTargetUrl(user, authentication);
		
		if(response.isCommitted())
			return;
		
		redirectStrategy.sendRedirect(request, response, targetUrl);
	}

	private String determineTargetUrl(User user, Authentication authentication) {
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		
		for (GrantedAuthority grantedAuthority : authorities) {
			if (EnumRoleUser.ROLE_ADMINISTRATOR.getName().equals(grantedAuthority.getAuthority())) {
				return "/administrator/dashboard?idRole=1&inSession=1";
			} else if (EnumRoleUser.ROLE_VALIDATOR.getName().equals(grantedAuthority.getAuthority())) {
				return "/validator/dashboard?idRole=2&inSession=1";
			} else if (EnumRoleUser.ROLE_CANDIDATE.getName().equals(grantedAuthority.getAuthority())) {
				return "/candidate/dashboard?idRole=3&inSession=1";
			}
		}

		return SecurityConfiguration.LOGOUT_DENIED_PERMISSION;
	}
	
	protected void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if(session == null)
			return;
		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}
}