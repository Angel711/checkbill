package net.pwa.checkbill.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CustomUserDetails extends User {
	
	private static final long serialVersionUID = 1L;
	
	private Long idUser;
	
	public CustomUserDetails(String username, String password, boolean enabled, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, true, true, accountNonLocked, authorities);
	}
}