package net.pwa.checkbill.exception;

import lombok.Getter;
import lombok.Setter;
import net.pwa.checkbill.enums.msg.ErrorMessage;


@Getter
@Setter
public class CustomException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	private ErrorMessage msgError;
	
	public CustomException(Exception e) {
		super(e);
	}
	
	public CustomException(String message) {
		super(message);
	}
	
	public CustomException(ErrorMessage msgError) {
		this.msgError = msgError;
	}
	
	public CustomException(ErrorMessage msgError, Exception e) {
		super(e);
		this.msgError = msgError;
	}
}