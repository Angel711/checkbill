<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<!-- RESOURCES -->
		<%@ include file="/sources/templates/fixed_resources.jsp"%>
		
		<!-- MINE CSS -->
		<link type="text/css" rel="stylesheet" href="<c:url value="/sources/css/error.css" />" media="screen,projection" />
		
		<!-- TITLE -->
		<title>${contextTitle} - ERROR 403</title>
	</head>
	<body>
		<!-- MAIN -->
		<main>
			<div class="container">
				<div class="row">
					<div class="card col s12 m8 offset-m2 blue-grey darken-1 hoverable white-text center card-main-x10 z-depth-5">
						<div class="card-content">
							<div class="row center">
								<hr>
								<h1>ERROR 403</h1>
								<hr>
							</div>
							<div class="container">
								<div class="row center">
									<h5>Permiso denegado.<br>Lo sentimos, no tienes permiso suficiente para poder proceder con la petici�n.</h5>
									<br>
								</div>
								<div class="row">
									<c:if test="${message != null}">
										<h5>Esta p�gina esta fallando Estas pueden ser las razones: </h5>
										<ul class="browser-default">
											<li>${message}</li>
										</ul>
									</c:if>
									<br><br>
								</div>
								<div class="row center">
									<a class="btn-large waves-effect waves-light btn-danger pulse hoverable btnGoBack">�Volver!</a>
								</div>
							</div>
						</div>
					</div>
				</div>
	    	</div>
		</main>
	</body>
</html>