<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<!-- RESOURCES -->
		<%@ include  file="/sources/templates/fixed_resources.jsp" %>
		
		<!-- MINE CSS -->
		<link type="text/css" rel="stylesheet" href="<c:url value="/sources/css/login.css" />" media="screen,project" />
		
		<title>Inicio de sesion</title>
	</head>
	<body>
		<!-- HEADER -->
<%-- 		<%@ include  file="/sources/templates/header.jsp" %> --%>

		<!-- MAIN -->
            <main>
                <div class="container">
                    <div class="row">
                        <div class="col s12 m10 offset-m1 l8 offset-l2 xl6 offset-xl3">
                            <div class="card login indigo darken-4 white-text">
                                <div class="card-content">
                                    <div class="row center">
                                        <span class="card-title">Login</span>
                                    </div>
                                    <div class="row">
                                        <c:url var="loginURL" value="/api/authenticate" />
                                        <form action="${loginURL}" method="post" id="formLogin">
                                            <div class="row">
                                                <div class="input-field col s12 m8 offset-m2 black-text">
                                                    <input id="username" name="username" type="text" class="black-text validate">
                                                    <label for="username">Email <span class="required">*</span></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m8 offset-m2">
                                                    <input id="password" name="password" type="password" class="black-text validate">
                                                    <label for="password">Password <span class="required">*</span></label>
                                                </div>
                                            </div>
                                            <div class="row center">
                                                <button class="btn waves-effect waves-light"><i class="material-icons right">send</i>Entrar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
		
		<!-- FOOTER -->
		<%@ include  file="/sources/templates/footer.jsp" %>
		
		<!-- GET MESSAGE -->
		<c:if test="${param.logout != null}">
			<c:set scope="request" var="userMessage" value="Has cerrado sesin exitosamente."></c:set>
		</c:if>
		
		<!-- GLOBAL VARIABLE JAVASCRIPT -->
		<script type="text/javascript" charset="utf-8">
			var USER_MESSAGE = '${userMessage}';
		</script>
		
		<!-- MINE JS -->
		<script charset="utf-8" type="text/javascript" src="<c:url value="/sources/js/validations/validations.js" />"></script>
		<script charset="utf-8" type="text/javascript" src="<c:url value="/sources/js/validations/password.js" />"></script>
		<script type="text/javascript" charset="utf-8" src="<c:url value="/sources/js/password.js" />"></script>
	</body>
</html>
