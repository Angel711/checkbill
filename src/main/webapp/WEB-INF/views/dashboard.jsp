<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<!-- RESOURCES -->
		<%@ include  file="/sources/templates/fixed_resources.jsp" %>
		
		<!-- MINE CSS -->
<%-- 		<link type="text/css" rel="stylesheet" href="<c:url value="/sources/css/login.css" />" media="screen,project" /> --%>
		
		<title>${contextTitle} - Dashboard</title>
	</head>
	<body>
		<!-- HEADER -->
<%-- 		<%@ include  file="/sources/templates/header.jsp" %> --%>

		<!-- MAIN -->
		<main>
			<div class="container">
				<div class="row">
					<div class="col s12">
						<div class="card hoverable">
							<div class="card-content">
								<div class="container">
									<div class="row center">									
										<span class="card-title">NOTICIAS: ${currentDay}</span>
									</div>
									<div class="row">
										<h6>Hola mundo</h6>
									</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		
		<!-- FOOTER -->
		<%@ include  file="/sources/templates/footer.jsp" %>
		
		<!-- GET MESSAGE -->
		<c:if test="${param.logout != null}">
			<c:set scope="request" var="userMessage" value="Has cerrado sesi�n exitosamente."></c:set>
		</c:if>
		
		<!-- GLOBAL VARIABLE JAVASCRIPT -->
		<script type="text/javascript" charset="utf-8">
			var USER_MESSAGE = '${userMessage}';
		</script>
		
		<!-- MINE JS -->
		<script type="text/javascript" charset="utf-8" src="<c:url value="/sources/js/login.js" />"></script>
	</body>
</html>