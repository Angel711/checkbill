<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<!-- RESOURCES -->
		<%@ include  file="/sources/templates/fixed_resources.jsp" %>
		
		<!-- MINE CSS -->
<%-- 		<link type="text/css" rel="stylesheet" href="<c:url value="/sources/css/login.css" />" media="screen,project" /> --%>
		
		<title>${contextTitle} - Password</title>
	</head>
	<body>
		<!-- HEADER -->
		<%@ include  file="/sources/templates/header.jsp" %>

		<!-- MAIN -->
		<main>
			<div class="container">
				<div class="row">
					<div class="col s12 m10 offset-m1 l8 offset-l2 xl6 offset-xl3">
						<div class="card hoverable">
							<div class="card-content">
								<div class="row center">									
									<span class="card-title">CAMBIO DE CONTRASEA</span>
								</div>
								<div class="row">
									<form id="formChangePassword">
										<div class="row">
											<div class="input-field col s12 m8 offset-m2">
												<input id="password" name="password" type="password"> 
												<label for="password">Contrasea <span class="required">*</span></label>
        									</div>
										</div>
										<div class="row">
											<div class="input-field col s12 m8 offset-m2">
												<input id="passwordConfirmation" name="passwordConfirmation" type="password"> 
												<label for="passwordConfirmation">Confirmacin de contrasea <span class="required">*</span></label>
        									</div>
										</div>
										<div class="row center">
											<button type="button" class="btn waves-effect red waves-light"><i class="material-icons right">close</i>limpiar</button>
											<button class="btn waves-effect blue waves-light"><i class="material-icons right">send</i>envar</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		
		<!-- FOOTER -->
		<%@ include  file="/sources/templates/footer.jsp" %>
		
		<!-- MINE JS -->
		<script charset="utf-8" type="text/javascript" src="<c:url value="/sources/js/validations/validations.js" />"></script>
		<script charset="utf-8" type="text/javascript" src="<c:url value="/sources/js/validations/password.js" />"></script>
		<script type="text/javascript" charset="utf-8" src="<c:url value="/sources/js/password.js" />"></script>
	</body>
</html>
