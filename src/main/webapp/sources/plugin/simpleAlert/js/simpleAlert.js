/*
=====================================================
==================== SIMPLE ALERT ===================
=====================================================
|													|
| How to use?										|
| 	To use the simpleAlert plugin, you have to call |
| it like this: $.simpleAlert(), but the            |
| simpleAlter will appear like default. The plugin  |
| has severl options to customize:                  |
| 1. title: This is the title for the simpleAlert,  | 
| so write what you wnat.                           |
| 2. text: this is the text for the simpleAlert,    |
| so write what you want.                           |
| 3. type: This option will be to show a            |
| simpleAlert with a specific style. The default    |
| option is "success". There are four of them:      |
|   * error                                         |
|   * success                                       |
|   * notification                                  |
|   * warning                                       |
| 4. theme: This option as well like type will be   |
| to show a simpleAlert with a specific style. The  |
| default option is "dark". There are two of them:  |
|   * light                                         |
|   * dark                                          |
| 5. delay: This option is the time that            |
| impleAlert will be visible. The value for this    |
| is in milliseconds. If it'll get -1 the           |
| simpleAlert won't be removed from the screen      |
| automatically. The default value is 5000          |
| milliseconds.                                     |
=====================================================
*/
(function() {
    // Variables
    var 
        simpleAlertHTML = '',
        simpleAlertCounter = 0,
        simpleAlertID = undefined,
        simpleAlertStyle = undefined,
        simpleAlertContainerClass = '.simpleAlert-container';

   // Plugin declaration
    $.simpleAlert = function(options) {
        options = $.extend({
            title: 'Simple Alert',
            text: ' This is an alert from SimpleAlert plugin.',
            type: 'success',
            types: [
                'error',
                'warning',
                'success',
                'notification'
            ],
            theme: 'dark',
            themes: [
                'dark',
                'light'
            ],
            delay: 5000
        }, options);

        // Select a style
        simpleAlertStyle = 'simpleAlert-';

        if(options.types.includes(options.type)) {
            simpleAlertStyle += options.type + '-'
        } else {
            simpleAlertStyle += 'success-'
        }

        if(options.themes.includes(options.theme)) {
            simpleAlertStyle += options.theme;
        } else {
            simpleAlertStyle += 'dark'
        }

        // Create or search container
        if($(simpleAlertContainerClass).length === 0) {
            $('body').append('<div class="simpleAlert-container"></div>');
        }

        // Create an identifier
        simpleAlertCounter++;
        simpleAlertID = 'simpleAlert-id-' + simpleAlertCounter;

        // HTML Code
        simpleAlertHTML += '<div class="simpleAlert ' + simpleAlertStyle + '" id="' + simpleAlertID + '">'
        simpleAlertHTML += '    <a href="#"> X </a>'
        simpleAlertHTML += '    <p>' + options.title + '</p>'
        simpleAlertHTML += '    <span>' + options.text + '</span>'
        simpleAlertHTML += '</div>'
        $(simpleAlertContainerClass).append(simpleAlertHTML);
        simpleAlertHTML = '';
        showSimpleAlert($('#' + simpleAlertID));

        // Close alert automatically
        if(options.delay >= 0) {
            setTimeout(function() {
                hideSimpleAlert($('#' + simpleAlertID));
            }, options.delay);
        }

        // Close alert manually
        $('.simpleAlert').on('click', 'a', function() {
            hideSimpleAlert($(this).parent());
        });
    };

    function showSimpleAlert($simpleAlert) {
        $simpleAlert.slideDown();
    }

    function hideSimpleAlert($simpleAlert) {
        $simpleAlert.slideUp(function() {
            $simpleAlert.remove();
        });
    }
})();