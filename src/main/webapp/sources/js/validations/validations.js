var flagErrorRecursion = true;

jQuery.validator.addMethod("requiredFalse", function(value, element) {	
	var $element = $("#" + element.id);
	
	if(!($element.hasClass('select2'))) {
		if($element.val().trim().length == 0)
			$element.val('');		
	}
	
	return true;
}, "Ingrese este campo");

jQuery.validator.addMethod("greaterThan", function(value, element, params) {
	if(value != undefined && value != null && value != '' && $(params).val() != undefined && $(params).val() != null && $(params).val() != '') {
		if((!/Invalid|NaN/.test(moment(value, "DD/MM/YYYY"))) && (!/Invalid|NaN/.test(moment($(params).val(), "DD/MM/YYYY")))) {
			return (moment(value, "DD/MM/YYYY").isSameOrAfter(moment($(params).val(), "DD/MM/YYYY")));	
		} else {
			return false
		}
	} else {
		return true;
	}
},'Must be greater than {0}.');

$.validator.setDefaults({
	normalizer: function(value) {
		return $.trim(value);
	},
	errorElement: 'div',
    errorPlacement: function(error, element) {
    	if(flagErrorRecursion) {
    		flagErrorRecursion = false;
    		element.parents().find('form').valid();
    	} else {    		
    		var select = (element[0].localName);
    		if(select === "select" && (!($(element).hasClass('select2')))) {
    			var $input = $(element).parent().find("input");
    			
    			if(!($input.hasClass("error"))) 
    				$input.addClass("validate error");
    		}
    		
    		// SELECT2
    		var elem = $(element);
    		if (elem.hasClass("select2-hidden-accessible")) {
    			$elemBase = $("#select2-" + elem.attr("id") + "-container");
    			
    			if($elemBase.parent().hasClass('select2-selection--single')) {
    				$elemBase.addClass("error");
    				element = $elemBase.parent(); 
    				error.insertAfter(element);
    			} else {
    				var $parent = elem.parent().find('span.select2.select2-container.select2-container--default'),
    				$object = $parent.find('span.select2-selection.select2-selection--multiple');
    				
    				$object.addClass('error');
    				element = $object.parent(); 
    				error.insertAfter(element);
    			}
    		} else {
				if (element.is(":radio")) {
					var id = element.data('id');
					
					if(!element.data('otro'))
						error.appendTo(element.parent().parent().parent());
					
					if(id != null && id != undefined) {
						if($('#inputOtro' + id).attr('disabled') != undefined)
							error.appendTo(element.parent().parent().parent());
					} else {						
	                	error.appendTo(element.parent().parent().parent());
					}
	            } else { 
	                error.insertAfter(element);
	            }
    		}    		
    	}
    },
    success: function(label) {
		var select = (label.prev('select')),
			field = (label.prev('input'));
		
		setTimeout(function() {	
			if (field.is(":radio")) {
				var id = field.data('id');
				
				if(id != null && id != undefined) {
					if($('#inputOtro' + id).attr('disabled') != undefined)
						field.removeClass('invalid').removeClass('error').addClass('valid');
				} else {						
                	field.removeClass('invalid').removeClass('error').addClass('valid');
				}
            } else { 
                field.removeClass('invalid').removeClass('error').addClass('valid');
            }		
		}, 100);
		
		if(select[0] != undefined) {
			if(select[0].localName === 'select') {
				var $input = select.prev().prev().prev();
				$input.removeClass('error').addClass('valid');
			}
		}
		
		// SELECT2 SINGLE
		if(label.parent().hasClass('selection')) 
			var $objectSingle = label.parent().find('span.select2-selection.select2-selection--single').find('span.select2-selection__rendered').addClass('valid').removeClass('error');
		
		// SELECT2 MULTIPLE
		if(label.parent().find('span.selection').length > 0);
			var $objectMultiple = label.parent().find('span.selection').find('span.select2-selection.select2-selection--multiple').addClass('valid').removeClass('error');
		
        label.html("Correcto").addClass("checked");
    }
});
