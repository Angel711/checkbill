$("#formLogin").validate({
	rules: {
		username: {
			required: true
		},
		password: {
			required: true
		}
	},
	messages: {
		username: {
			required: "Ingrese su correo o teléfono"
		},
		password: {
			required: "Ingrese su contraseña"
		} 
	}
});
