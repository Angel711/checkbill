$("#formChangePassword").validate({
	rules: {
		password: {
			required: true,
			minlength: 5,
			maxlength: 50
		},
		passwordConfirmation: {
			required: true,
			minlength: 5,
			maxlength: 50,
			equalTo: '#password'
		}
	},
	messages: {
		password: {
			required: "Ingrese su contraseña",
			minlength: "No puede ingresar menos de 5 carácteres",
			maxlength: "No puede ingresar más de 50 carácteres"
		},
		passwordConfirmation: {
			required: "Ingrese su contraseña nuevamente",
			minlength: "No puede ingresar menos de 5 carácteres",
			maxlength: "No puede ingresar más de 50 carácteres",
			equalTo: "Las contraseñas deben ser identicas"
		} 
	}
});