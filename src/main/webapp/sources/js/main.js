
var 
	$document = $(document),
	$simpleLoading = $(".simpleLoading");

$document.ready(function() {
	$simpleLoading.slideUp();
});


// custom function to serialize
$.fn.customSerialize = function() {
	var data = undefined,
		$form = $(this).eq(0);
		
	if($form.valid()) {
		$.each($form.find('input, textarea'), function(index, field) {
			$(this).val($(this).val().trim());
		});
		
		data = $form.serialize();
	}
	
	return data;
};

// ==================== AJAX ====================
$document.ajaxStart(function() {
	$simpleLoading.show();
});
$document.ajaxComplete(function() {
	$simpleLoading.delay(500).hide();
});
$document.ajaxError(function(event, xhr, settings, exc) {
	if (xhr.responseJSON == undefined) {
		var parsedResponse = JSON.parse(xhr.responseText);
		showErrorAlert(parsedResponse.result);
	} else {
		var parsedResponse = JSON.parse(xhr.responseJSON);
		showErrorAlert(parsedResponse.result);
	}
});

$document.ajaxSuccess(function(event, xhr, settings) {
	if (xhr.responseJSON != undefined && xhr.responseText != undefined) {
		var parsedResponse = JSON.parse(xhr.responseText);
		
		// cuando se necesita obviar el mensaje
		if(!((settings.url).indexOf("solicitud/guardarMotivo") >= 0)) {			
			showAlert(parsedResponse.status, parsedResponse.result);
			$simpleLoading.delay(500).hide();
		}
	}
});

// ========================== ALERTS ==========================
function showAlert(type, message) {
	switch(type) {
		case 'error':
			showAlertError(message);
			break;
		case 'success':
			showAlertSuccess(message);
			break;
		case 'warning':
			showAlertWarning(message);
			break;
		case 'notification':
			showAlertNotification(message);
			break;
	}
}

function showAlertError(message) {
	$.simpleAlert({
        type: "error",
        title: "ERROR",
        text: message,
    });
}

function showAlertSuccess(message) {
	$.simpleAlert({
        type: "success",
        title: "ÉXITO",
        text: message,
    });
}

function showAlertWarning(message) {
	$.simpleAlert({
        type: "warning",
        title: "ALERTA",
        text: message,
    });
}

function showAlertNotification(message) {
	$.simpleAlert({
        type: "notification",
        title: "INFORMACIÓN",
        text: message,
    });
}