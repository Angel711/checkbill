var
	$btnCleanForm = $("#btnCleanForm"),
	$btnUpdatePassword = $("#btnUpdatePassword"),
	$formChangePassword = $("#formChangePassword");
	
$btnUpdatePassword.on('click', function(e) {
	e.preventDefault();
	
	if($formChangePassword.valid()) {
		$.ajax({
			type: 'POST',
			url: CONTEXT_PLUS + 'password/update',
			data: $formChangePassword.customSerialize(),
			success: function(data, textStatus, jqXHR) {
				console.log(jqXHR);
				var parsedResponse = JSON.parse(jqXHR.responseText);
				
				
//				if(parsedReponse.status == 'success') {
					console.log(parsedResponse);
//				}
			}
		});
	} else {
		showAlertError("Debe de llenar todos los campos correctamente.");
	}
});

$btnCleanForm.on('click', function() {
	$formChangePassword[0].reset();
	$formChangePassword.find('div.error').remove();
	$formChangePassword.find('div.checked').remove();
	$formChangePassword.find('.validate').removeClass('error');
	$formChangePassword.find('.validate').removeClass('valid');
	$formChangePassword.find('.validate').removeClass('invalid');
});