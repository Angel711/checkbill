<header>
    <!-- MED AND LARGE SIZE SCREEN -->
    <nav class="hide-on-small-only">
        <div class="nav-wrapper">
            <img id="imgHeader" src="<c:url value=" /sources/img/logo.png " />">
            <a href="#" class="brand-logo center">Check Bill</a>
        </div>
    </nav>

    <!-- SMALL SIZE SCREEN -->
    <nav class="hide-on-med-and-up">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo center">Check Bill</a>
        </div>
    </nav>
</header>