<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<!-- ======================================= CSS ======================================= -->
<!-- CHARSET UTF-8 -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- GOOGLE ICONS -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<!-- RESPONSIVE TAG -->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<!-- FAVICON -->
<link type="image/png" rel="icon" href="<c:url value="/sources/img/favicon.ico" />">

<!-- CSS PLUGINS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link type="text/css" rel="stylesheet" href="<c:url value="/sources/css/simpleLoading.css" />" media="screen,project" />
<link type="text/css" rel="stylesheet" href="<c:url value="/sources/plugin/simpleAlert/css/simpleAlert.css" />" media="screen,project" />

<!-- MINE CSS -->
<link type="text/css" rel="stylesheet" href="<c:url value="/sources/css/main.css" />" media="screen,project" />

<!-- ======================================= VARIABLES ======================================= -->
<spring:url value="/" var="contextPath"/>
<c:set var="contextPath" value="${contextPath}" scope="request"/>

<c:set var="contextTitle" value="CheckBill" />

<c:set var="userListRoles" value="0" scope="request"/>
<sec:authorize access="isAuthenticated()" var="authenticatedUser" />
<sec:authorize access="hasRole('CANDIDATE')" >
	<c:set var="userListRoles" value="${userListRoles + 1}" scope="request"/>
	<c:set scope="request" var="contextRoleActive" value="${contextPath}candidate/"></c:set>
	<c:set scope="request" var="userRoleActive" value="3"></c:set>
</sec:authorize>
<sec:authorize access="hasRole('VALIDATOR')" >
	<c:set var="userListRoles" value="${userListRoles + 1}" scope="request"/>
	<c:set scope="request" var="contextRoleActive" value="${contextPath}validator/"></c:set>
	<c:set scope="request" var="userRoleActive" value="2"></c:set>
</sec:authorize>
<sec:authorize access="hasRole('ADMINISTRATOR')" >
	<c:set var="userListRoles" value="${userListRoles + 1}" scope="request"/>
	<c:set scope="request" var="contextRoleActive" value="${contextPath}administrator/"></c:set>
	<c:set scope="request" var="userRoleActive" value="1"></c:set>
</sec:authorize>


<!-- ======================================= JSP ======================================= -->
<%@ include  file="/sources/templates/simpleLoading.jsp" %>


<!-- ======================================= JS ======================================= -->
<!-- GLOBAL VARIABLES JAVASCRIPT -->
<script type="text/javascript">
	var EXCEPCION = '${excepcion}',
		CONTEXT_PATH = "${contextPath}",
		CONTEXT_PLUS = "${contextRoleActive}",
		USER_ROLE_ACTIVE = "${userRoleActive}",
		IS_AUTHENTICATED = "${authenticatedUser}";
</script>

<!-- JS PLUGINS -->
<script type="text/javascript" charset="utf-8" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript" charset="utf-8" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script type="text/javascript" charset="utf-8" src="<c:url value="/sources/plugin/simpleAlert/js/simpleAlert.js" />"></script>

<!-- MINE JS -->
<script type="text/javascript" charset="utf-8" src="<c:url value="/sources/js/main.js" />"></script>
